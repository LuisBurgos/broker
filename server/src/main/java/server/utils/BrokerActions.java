package server.utils;

/**
 * Created by luisburgos on 11/10/15.
 */
public class BrokerActions {
    public static final int FIND_SERVICE = 1;
    public static final int EXECUTE_SERVICE = 2;
    public static final int REGISTER_SERVICE = 3;
}
