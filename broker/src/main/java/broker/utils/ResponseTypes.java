package broker.utils;

/**
 * Created by luisburgos on 11/10/15.
 */
public class ResponseTypes {

    public static final int CONNECTED = 0;
    public static final int SERVICE_FOUND = 1;
    public static final int SERVICE_NOT_FOUND = 2;
    public static final int DISCONNECTED = 3;
    public static final int REQUEST_RECEIVED = 4;
    public static final int REQUEST_ERROR = 5;
}
